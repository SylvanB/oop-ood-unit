package week1src;

import java.util.UUID;

public class BankAccount {
    /**
     * accountNumber is marked as final to ensure this value
     * is immutable once it has been instantiated.
     */
    private final UUID accountNumber;
    private String accountName;
    private int balance;

    /**
     *  Parameterised Constructor for a BankAccount
     *
     * @param accountNumber
     *        Immutable UUID account number used to uniquely identify tbe account
     *
     * @param accountName
     *        Used to link a person to the bank account
     *
     * @param balance
     *        The initial balance of the bank account
     *
     * @throws IllegalArgumentException
     *         Thrown if any of the params would place the BankAccount into an invalid state
     *         such as a negative balance, a null or empty accountName, or null accountNumber
     */
    public BankAccount(UUID accountNumber, String accountName, int balance) {
        if (accountNumber == null) {
            throw new IllegalArgumentException("Provided account number was null");
        }
        if (accountName == null || accountName.isBlank()) {
            throw new IllegalArgumentException("Provided account number was null or empty");
        }
        if (balance < 0 ) {
            throw new IllegalArgumentException("Tried to instantiate a BankAccount with a negative balance");
        }

        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.balance = balance;
    }

    /**
     * Parameter-less constructor for a default bank account.
     */
    public BankAccount() {
        this.accountNumber = UUID.randomUUID();
        this.accountName = "Placeholder Name";
        this.balance = 0;
    }

    public UUID getAccountNumber() {
        return this.accountNumber;
    }

    public String getAccountName() {
        return this.accountName;
    }

    public void setAccountName(String accountName) {
        if (accountName == null || accountName.isBlank()) {
            throw new IllegalArgumentException("Provided account number was null or empty");
        }
        this.accountName = accountName;
    }

    /**
     *  Get the current balance of the bank account.
     *
     * @return A int representing the current balance
     */
    public int checkBalance() {
        return this.balance;
    }

    /**
     * Used to deposit money into the bank account.
     *
     * @param amount
     *        A non-negative value that will be deposited into the bank account.
     *
     * @throws IllegalArgumentException
     *         Thrown when the provided amount is less than or equal to 0
     */
    public void depositBalance(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Invalid deposit amount (under or equal to zero)");
        }
        this.balance += amount;
    }

    /**
     * Validation on the amount provided ensures that only positive amounts of money can be withdrawn from
     * an account. If a negative amount, or an invalid denomination is provided an IllegalArgumentException will
     * be thrown.
     *
     * @param amount
     *        Defines the amount to be withdrawn from the account
     *
     * @throws IllegalArgumentException
     *         When the withdrawal amount is less than $0
     *
     */
    public void withdrawBalance(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Withdrawal request was an invalid amount (less than or equal to $0.00)");
        }

        this.balance -= amount;
    }

}
