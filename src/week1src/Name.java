package week1src;

public class Name {
    private String firstName;
    private String middleName;
    private String surname;

    public Name(String fname, String mname, String sname) {
        this.firstName = fname;
        this.middleName = mname;
        this.surname = sname;
    }

    public Name(String fullname) {
        var splitName = fullname.split(" ");
        this.firstName = splitName[0];
        this.middleName = splitName[1];
        this.surname = splitName[3];
    }

    public String getFirstCommaLast() {
        return this.surname + ", " + this.firstName;
    }

    public String getFullName() {
        return String.format("%s %s %s",this.firstName, this.middleName, this.surname);
    }

    public String getFirstAndLastName() {
        return String.format("%s %s",this.firstName, this.surname);
    }

}
