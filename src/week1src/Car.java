package week1src;

public class Car {
    private String model;
    private int tankSize;
    private double manfMPG;
    private Name ownerName;

    public Car(String model, int tank, double mpg, Name owner) {
        this.model = model;
        this.tankSize = tank;
        this.manfMPG = mpg;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return this.model;
    }

    public void setTankSize(int tank) {
        this.tankSize = tank;
    }

    public int getTankSize() {
        return this.tankSize;
    }

    public double estimateDistance() {
        return tankSize * manfMPG * 0.22;
    }

}
