package week3Src;

import java.util.ArrayList;

public class Name {
    private String firstName;
    private String middleName;
    private String surname;
    private ArrayList<Car> cars = new ArrayList<Car>();

    public Name(String fname, String mname, String sname) {
        this.firstName = fname;
        this.middleName = mname;
        this.surname = sname;
    }

    public Name(String fullname) {
        var splitName = fullname.split(" ");
        this.firstName = splitName[0];
        this.middleName = splitName[1];
        this.surname = splitName[2];
    }

    public String getLastCommaFirst() {
        return String.format("%s, %s", this.surname, this.firstName);
    }

    public String getFullName() {
        return String.format("%s %s %s",this.firstName, this.middleName, this.surname);
    }

    public String getFirstAndLastName() {
        return String.format("%s %s",this.firstName, this.surname);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void AddCar(Car c) {
        this.cars.add(c);
    }

    public Car getCar(int instance) {
        return this.cars.get(instance - 1);
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void removeCar(int i) {
        if (this.cars.size() >= i) {
            this.cars.remove(i);
        }
    }
}
