package week3Src;

public class Car {
    private String model;
    private int tankSize;
    private double manfMPG;
    private Name ownerName;
    private static int instances = 0;
    private int instanceNumber;

    private double GPL = 0.22;

    public Car(String model, int tank, double mpg, Name owner) {
        this.model = model;
        this.tankSize = tank;
        this.manfMPG = mpg;
        this.ownerName = owner;
        this.instanceNumber = this.instances++;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return this.model;
    }

    public void setTankSize(int tank) {
        this.tankSize = tank;
    }

    public int getTankSize() {
        return this.tankSize;
    }

    public double estimateDistance() {
        return this.tankSize * this.manfMPG * this.GPL;
    }

    public boolean tankBigger(int size) {
        return this.tankSize > size ? true : false;
    }

    public Name getOwner() {
        return this.ownerName;
    }

    public int getInstanceNumber() {
        return instanceNumber;
    }

    public double getManfMPG() {
        return manfMPG;
    }
}

