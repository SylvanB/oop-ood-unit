package week3Src;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {

    private Name owner;

    @BeforeEach
    void setUp() {
        this.owner = new Name("Sylvan Jude Bowdler");

        this.owner.AddCar(new Car("Porsche", 5, 13, this.owner));
        this.owner.AddCar(new Car("Mercedes Benz", 10, 23, this.owner));
        this.owner.AddCar(new Car("BMW i5", 20, 30, this.owner));
        this.owner.AddCar(new Car("Ford Focus", 45, 5, this.owner));
        this.owner.AddCar(new Car("Mini Cooper", 60, 55, this.owner));
    }

    @Test
    void estimateDistanceShouldReturnCorrectValue() {
        for (var car: this.owner.getCars()) {
            var expectedMpg = car.getTankSize() * car.getManfMPG() * 0.22;
            var actualMpg = car.estimateDistance();
            assertEquals(expectedMpg, actualMpg);
        }
    }

    @Test
    void tankBiggerShouldReturnExpectedValue() {
        for (var car: this.owner.getCars()) {
            var expected = car.getTankSize() > 5;
            var actual = car.tankBigger(5);
            assertEquals(expected, actual);
        }
    }

    @Test
    void getLastCommaFirstShouldReturnCorrectlyFormattedName() {
        var expected = "Bowdler, Sylvan";
        var actual = this.owner.getLastCommaFirst();
        assertEquals(expected, actual);
    }

    @Test
    void get3rdCarInstanceForGivenName() {
        var expectedCar = "BMW i5";
        var actualCar = this.owner.getCar(3).getModel();
        assertEquals(expectedCar, actualCar);
    }
}