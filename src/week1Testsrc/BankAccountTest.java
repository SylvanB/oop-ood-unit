package week1Testsrc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import week1src.BankAccount;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class BankAccountTest {

    private BankAccount account;

    @BeforeEach
    void setUp() {
        this.account = new BankAccount(UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d"), "Bob Smith", 999);
    }

    @Test
    void bankAccountConstructorParameters() {
        var account = new BankAccount(UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d"), "Bob Jones", 100);

        var expectedAccountNumber = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
        var expectedAccountName = "Bob Smith";
        var expectedBalance = 999;

        assertEquals(account.getAccountNumber(), expectedAccountNumber);
        assertEquals(account.getAccountName(), expectedAccountName);
        assertEquals(account.checkBalance(), expectedBalance);
    }

    @Test
    void bankAccountConstructorDefault() {
        var account = new BankAccount();

        var expectedAccountName = "Placeholder Name";
        var expectedBalance = 0;

        assertTrue(account.getAccountNumber() != null);
        assertEquals(account.getAccountName(), expectedAccountName);
        assertEquals(account.checkBalance(), expectedBalance);
    }

    @Test
    void getAccountNumber() {
        var expected = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
        assertEquals(this.account.getAccountNumber(), expected);
    }

    @Test
    void getAccountName() {
        var expected = "Bob Smith";
        assertEquals(this.account.getAccountName(), expected);
    }

    @Test
    void setAccountName_WithValidName() {
        this.account.setAccountName("John Smith");
        var expected = "John Smith";
        assertEquals(this.account.getAccountName(), expected);
    }

    @Test
    void setAccountName_WithNullName() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            this.account.setAccountName(null);
        });

        var expectedMessage = "Provided account number was null or empty";
        var actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void setAccountNameWith_EmptyName() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            this.account.setAccountName("");
        });

        var expectedMessage = "Provided account number was null or empty";
        var actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void checkBalance() {
        var expected = 999.00;
        assertEquals(this.account.checkBalance(), expected);
    }

    @Test
    void depositBalance_WithValidAmount() {
        this.account.depositBalance(100);
        assertEquals(this.account.checkBalance(), 1099);
    }

    @Test
    void depositBalance_NegativeAmount() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            this.account.depositBalance(-1);
        });

        var expectedMessage = "Invalid deposit amount (under or equal to zero)";
        var actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void depositBalance_ZeroAmount() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            this.account.depositBalance(0);
        });

        var expectedMessage = "Invalid deposit amount (under or equal to zero)";
        var actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void withdrawBalance_ValidAmount() {
        this.account.withdrawBalance(100);
        assertEquals(this.account.checkBalance(), 899);
    }

    @Test
    void withdrawBalance_ZeroAmount() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            this.account.withdrawBalance(0);
        });

        var expectedMessage = "Withdrawal request was an invalid amount (less than or equal to $0.00)";
        var actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void withdrawBalance_NegativeAmount() {
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            this.account.withdrawBalance(-10);
        });

        var expectedMessage = "Withdrawal request was an invalid amount (less than or equal to $0.00)";
        var actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}