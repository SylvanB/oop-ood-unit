package week1Testsrc;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import week1src.Car;

class CarTest {

    private Car testCar;

    @BeforeEach
    public void setUp() {
        // Sets the testCar field to a known good value before each testcase
        this.testCar = new Car("Ford", 40, 14.44);
    }

    @Test
    void testCarConstructor() {
        // Values passed to the constructor should be set on the Car object

        var tankSize = this.testCar.getTankSize();
        var model = this.testCar.getModel();

        var expectedDistance = tankSize * 14.44 * 0.22;

        assertEquals(model, "Ford");
        assertEquals(tankSize, 40);
        assertEquals(this.testCar.estimateDistance(), expectedDistance);
    }

    @Test
    void testCarGetTankSize() {
        // Ensuring that the correct tankSize value is returned

        var tankSize = this.testCar.getTankSize();

        assertEquals(tankSize, 40);
    }

    @Test
    void testCarSetTankSize() {
        // Ensuring that the tankSize is updated after a call to the tankSize setter

        this.testCar.setTankSize(999);
        var tankSize = this.testCar.getTankSize();

        assertEquals(tankSize, 999);
    }

    @Test
    void testCarGetModel() {
        // Ensuring that the correct model is returned when calling the getter

        var tankSize = this.testCar.getModel();

        assertEquals(tankSize, "Ford");
    }

    @Test
    void testCarSetModel() {
        // Ensuring that the model is updated after a call to the model setter

        this.testCar.setModel("Ferrari");
        var tankSize = this.testCar.getModel();

        assertEquals(tankSize, "Ferrari");
    }

    @Test void testCarEstimateDistance() {
        var expectedDistance = this.testCar.getTankSize() * 14.44 * 0.22;

        assertEquals(this.testCar.estimateDistance(), expectedDistance);
    }

}